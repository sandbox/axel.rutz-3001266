<?php

namespace Drupal\dcv\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'dcv_settings_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "dcv_formatter",
 *   label = @Translation("DCV Formatter"),
 *   field_types = {
 *     "dcv_settings"
 *   }
 * )
 */
class DCVSettingsFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
             // Implement default settings.
           ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
             // Implement settings form.
           ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    $value = $item->value;
    $settings = !empty($value) ? unserialize($value) : [];
    $entity = \Drupal::service('dcv.helper')->getView($settings);
    $view = $entity->getExecutable();
    $view->setDisplay('default');
    $view->initDisplay();
    $view->preExecute();
    $view->execute();
    $build = $view->render();
    $html = render($build);

    return $html;
  }

}
