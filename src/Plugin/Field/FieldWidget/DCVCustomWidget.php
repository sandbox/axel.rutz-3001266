<?php

namespace Drupal\dcv\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Entity\View;

/**
 * Plugin implementation of the 'dcv_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "dcv_custom_widget",
 *   label = @Translation("DCV Custom UI"),
 *   field_types = {
 *     "dcv_settings"
 *   }
 * )
 */
class DCVCustomWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items->getValue();

    // Get the views compatible settings array.
    $settings = !empty($value[0]['value']) ? unserialize($value[0]['value']) : [];
    if (empty($settings)) {
      $view_entity = View::load('dynamic_content_view');
      $display = $view_entity->getDisplay('default');
      $settings = $display['display_options'];
    }

    $element['settings_form'] = $this->getBaseForm();

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $views_store = \Drupal::service('tempstore.shared')->get('views');
    $tempstore = $views_store->get('dynamic_content_view');
    if (!empty($tempstore)) {
      $display = $tempstore->get('display');
      $settings = $display['default']['display_options'];
      $views_store->delete('dynamic_content_view');
      $items->setValue(serialize($settings));
    }
  }

  /**
   * Get the base form for the settings.
   */
  public function getBaseForm() {
    $view_modes = [];
    $view_mode_entities = \Drupal::entityQuery('entity_view_mode')
       ->condition('targetEntityType', 'node')
       ->execute();
    foreach ($view_mode_entities as $view_mode) {
      $parts                  = explode('.', $view_mode);
      $view_modes[$view_mode] = $parts[1];
    }
    return [
      'filters' => [
        '#type' => 'details',
        '#title' => t('Filters'),
        'content' => [
          '#markup' => 'Filters placeholder.'
        ],
      ],
      'sorts' => [
        '#type' => 'details',
        '#title' => t('Sorts'),
        'content' => [
          '#markup' => 'Sorts placeholder.'
        ],
      ],
      'view_mode' => [
        '#type' => 'select',
        '#title' => t('View mode'),
        '#options' => $view_modes,
      ],
      'pager' => [
        '#type' => 'details',
        '#title' => t('Pager'),
        'content' => [
          '#markup' => 'Pager placeholder.'
        ],
      ],
    ];
  }

}
