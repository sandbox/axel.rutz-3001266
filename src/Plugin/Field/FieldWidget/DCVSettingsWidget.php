<?php

namespace Drupal\dcv\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'dcv_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "dcv_widget",
 *   label = @Translation("DCV Views UI"),
 *   field_types = {
 *     "dcv_settings"
 *   }
 * )
 */
class DCVSettingsWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items->getValue();
    $settings = !empty($value[0]['value']) ? unserialize($value[0]['value']) : [];
    /** @var \Drupal\dcv\DCVHelper $dcvHelper */
    $dcvHelper = \Drupal::service('dcv.helper');
    $element['settings_form'] = $dcvHelper->getViewsForm($settings, 'views_restricted_simple');

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {
    $views_store = \Drupal::service('tempstore.shared')->get('views');
    $tempstore = $views_store->get('dynamic_content_view');
    if (!empty($tempstore)) {
      $display = $tempstore->get('display');
      $settings = $display['default']['display_options'];
      $views_store->delete('dynamic_content_view');
      $items->setValue(serialize($settings));
    }
  }

}
