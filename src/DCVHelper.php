<?php

namespace Drupal\dcv;

use Drupal\Core\Controller\ControllerBase;
use Drupal\views_restricted\ViewsRestrictedHelper;
use Drupal\views_ui\ViewUI;
use Drupal\views\Entity\View;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class DCVHelper.
 */
class DCVHelper extends ControllerBase {

  /**
   * Entity manage class.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a new DCVHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   Entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entityManager) {
    $this->entityManager = $entityManager;
  }

  /**
   * Generate the form for DCV.
   *
   * @param array $settings
   *   Settings for dcv.
   *
   * @return array
   *   Form element.
   */
  public function getViewsForm(array $settings, $viewsRestrictedId) {
    $view = $this->getView($settings);
    $element = [
      '#type' => 'views_restricted',
      '#view' => $view,
      '#display' => 'default',
      '#views_restricted' => $viewsRestrictedId,
      '#attached' => ['library' => ['dcv/admin']],
    ];
    return $element;
  }

  /**
   * Constructs the view from base and settings.
   *
   * @param array $settings
   *   Array of default display settings.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\views\Entity\View|null
   *   View entity.
   */
  public function getView(array $settings) {
    $entity = View::load('dynamic_content_view');
    if (!empty($settings)) {
      $display = $entity->getDisplay('default');
      $display['display_options'] = $settings;
      $displays = ['default' => $display];
      $entity->set('display', $displays);
    }

    return $entity;
  }

}
